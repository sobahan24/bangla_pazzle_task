<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Subcategory;
use App\Models\Category;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('subcategory')->get();
        $subcategories = Subcategory::all();
        return view('admin.products.index', compact('products', 'subcategories'));
    }

    public function show($id)
    {
        // $products = Product::with('subcategory')->get();
        $products = Product::where('id', $id)->first();
        $subcategories = Subcategory::all();
        return view('admin.products.show', compact('products', 'subcategories'));
    }

    public function create()
    {
        $subcategories = Subcategory::all();
        return view('admin.products.create', compact('subcategories'));
    }

    public function store(Request $request)
    {
        $product = new Product();
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('assets/uploads/product/',$filename);
            $product->image = $filename;
        }
        $product->name = $request->input('name');
        $product->slug = $request->input('slug');
        $product->small_description = $request->input('small_description');
        $product->description = $request->input('description');
        $product->original_price = $request->input('original_price');
        $product->selling_price = $request->input('selling_price');
        $product->tax = $request->input('tax');
        $product->qty = $request->input('qty');
        $product->subcategory_id = $request->input('subcategory_id');
        $product->save();

        return redirect()->route('products.index')->with('status', "Product Added Successfully");
    }

    public function edit(Product $product)
    {
        $subcategories = Subcategory::all();
        return view('admin.products.edit', compact('product', 'subcategories'));
    }

    public function update(Request $request, Product $product)
    {
        if($request->hasFile('image'))
        {
            $path = 'assets/uploads/products/'.$product->image;
            if(File::exists($path))
            {
                File::delete($path);
            }
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('assets/uploads/product/',$filename);
            $product->image = $filename;
        }
        $product->name = $request->input('name');
        $product->slug = $request->input('slug');
        $product->small_description = $request->input('small_description');
        $product->description = $request->input('description');
        $product->original_price = $request->input('original_price');
        $product->selling_price = $request->input('selling_price');
        $product->tax = $request->input('tax');
        $product->qty = $request->input('qty');
        $product->subcategory_id = $request->input('subcategory_id');
        $product->save();

        return redirect()->route('products.index')->with('status', "Product Updated Successfully");
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')->with('status', "Product Deleted Successfully");
    }



}
