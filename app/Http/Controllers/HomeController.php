<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $subcategories = Subcategory::all();
        $categories = Category::all();
        $products = Product::all();
        return view('frontend.home', compact('categories', 'subcategories', 'products'));
    }

    public function view_products($id)
    {
        if (Product::where('subcategory_id', $id)->exists()) {
            $subcategories = Subcategory::where('id', $id)->first();
            $categories = Category::all();
            $subcategoriesAll = Subcategory::all();
            $productsAll = Product::all();
            $products = Product::where('subcategory_id', $id)->get();
            return view('frontend.products.index', compact('products', 'categories', 'subcategories', 'productsAll', 'subcategoriesAll'));
        } else {
            return redirect('/')->with('status', "products does not exists");
        }
    }
    public function single_product_details($id)
    {
        $products = Product::where('id', $id)->first();
        $subcategories = Subcategory::all();
        $categories = Category::all();
        $productsAll = Product::all();
        $subcategoriesAll = Subcategory::all();
        return view('frontend.products.details', compact('products', 'subcategories','categories', 'productsAll', 'subcategoriesAll'));
    }
}
