-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 26, 2023 at 09:15 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bangla_pazzle`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Women\'s & Girls\' Fashion', '2023-08-25 22:07:30', '2023-08-25 22:07:30'),
(2, 'Health & Beauty', '2023-08-25 22:28:14', '2023-08-25 22:28:14'),
(3, 'Men\'s & Boys\' Fashion', '2023-08-25 22:41:28', '2023-08-25 22:41:28'),
(4, 'Mother & Baby', '2023-08-25 22:51:23', '2023-08-25 22:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_08_23_131903_create_categories_table', 1),
(6, '2023_08_23_132219_create_subcategories_table', 1),
(7, '2023_08_23_132422_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint UNSIGNED NOT NULL,
  `subcategory_id` bigint NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selling_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `subcategory_id`, `name`, `slug`, `small_description`, `description`, `original_price`, `selling_price`, `image`, `qty`, `tax`, `created_at`, `updated_at`) VALUES
(1, 1, 'Washable Foot Sofa Shoes for Women', 'Washable Foot Sofa Shoes for Women', 'FAIR-P530 Washable Foot Sofa Shoes for Women New Casual Shoes', 'FAIR-P530 Washable Foot Sofa Shoes for Women New Casual Shoes Clearance Women and Girls Fashion General Bark Solid Flat Single Shallow Casual Shoes\r\nSize - 37-38-39-40\r\nShoe measurements\r\nFair 37 = Bata 4, Apex 37, Inches 9.5, Centimeters 24.4\r\nFair 38 = Bata 5 , Apex 38 , Inch 10 , Centimeter 25.4\r\nFair 39 = Bata 6, Apex 39, Inches 10.2, Centimeters 26\r\nFair 40 = Bata 7 , Apex 40 , Inch 10.7 , Centimeter 27', '1200', '950', '1693023673.jpg', '1', '1', '2023-08-25 22:21:13', '2023-08-26 02:03:47'),
(2, 1, 'Fashionable Flat Shoes for Women', 'Fashionable Flat Shoes for Women', 'Everyday Essentials: Comfortable and Fashionable Flat Shoes for Women', 'Everyday Essentials: Comfortable and Fashionable Flat Shoes for Women\r\n\r\nSole Material: Rubber - Lightweight and Wear-resistant sole.\r\nUpper Material : Breathable Textile/ Fabric Applicable object: 18-40 years old\r\nSuitable For Season: Wearable in all seasons.', '1100', '920', '1693023807.jpg', '1', '1', '2023-08-25 22:23:27', '2023-08-26 02:05:43'),
(3, 2, 'School Bag For Girls', 'School Bag For Girls', 'Fashonable Bag For Women - School Bag For Girls\r\nProduct Type: Backpack(without Doll) For Girl', 'Fashonable Bag For Women - School Bag For Girls\r\n\r\nProduct Type: Backpack(without Doll) For Girl\r\nGender: Women\r\nYou Get this backpack Bag without Doll.\r\nStyle: New style Fashion\r\nClosure type: Zipper', '2000', '1650', '1693023940.jpg', '1', '1', '2023-08-25 22:25:40', '2023-08-25 22:25:40'),
(4, 2, 'Waterproof Backpacks', 'Waterproof Backpacks', 'Waterproof Embroidery Multifunctional Anti-theft Backpacks Oxford Cloth Shoulder Begs For Teenagers Girls Large Capacity Travel School', 'Waterproof Embroidery Multifunctional Anti-theft Backpacks Oxford Cloth Shoulder Begs For Teenagers Girls Large Capacity Travel School\r\n\r\nGender: Women\r\nMaterial: PU Leather\r\nComfortable and Fashionable\r\nStylish and Beautiful Look\r\nColours: Black,Blue\r\nGender: Women\r\nMaterial: PU Leather\r\nComfortable and Fashionable', '2100', '1750', '1693024034.jpg', '1', '1', '2023-08-25 22:27:14', '2023-08-25 22:27:14'),
(5, 3, 'Pimples Remover Black Mud Mask', 'Pimples Remover Black Mud Mask', 'Bioaqua Blackhead Deep cleansing Purifying Peel Acne Pimples Remover Black Mud Mask skin care product- 60g face mask', 'Bioaqua Blackhead Deep cleansing Purifying Peel Acne Pimples Remover Black Mud Mask skin care product- 60g face mask\r\n\r\nProduct Type: Blackheads Remover\r\nBrand: BIOAQUA\r\nNet Weight: 60g\r\nShelf Life: 3 Years\r\nForm: Cream\r\nSuitable for: All Skin', '800', '650', '1693024284.jpg', '1', '1', '2023-08-25 22:31:24', '2023-08-25 22:31:24'),
(6, 3, 'Sada chondon gura  - 50 gm', 'Sada chondon gura  - 50 gm', 'White sandalwood powder - 50 gm - Sada chondon gura', 'White sandalwood powder - 50 gm - Sada chondon gura\r\n\r\nSandalwood powder is produced when a stick of sandalwood – a yellow and highly fragrant wood is ground down into powder.\r\nPrevents Acne\r\nAnti-Ageing Properties\r\nSpeeds Up Healing\r\nTreats Sunburn\r\nMoisturizes The skin\r\nWhitens the skin', '750', '550', '1693024585.jpg', '1', '1', '2023-08-25 22:36:25', '2023-08-26 02:06:36'),
(7, 4, 'Parachute Naturale Shampoo', 'Parachute Naturale Shampoo', 'Parachute Naturale Shampoo Nourishing Care 340ml', 'Parachute Naturale Shampoo Nourishing Care 340ml\r\n\r\nBrand: Parachute Naturale\r\nProduct Type: Shampoo\r\nVariant: Nourishing Care\r\nEnriched with the goodness of Coconut Milk and Aloe Vera\r\nDermatologically Tested\r\nParaben Free*', '950', '650', '1693024691.jpg', '1', '1', '2023-08-25 22:38:11', '2023-08-25 22:38:11'),
(8, 4, 'Tresemme Shampoo 580ml', 'Tresemme Shampoo 580ml', 'Tresemme Shampoo Hair Fall Defense 580ml', 'Tresemme Shampoo Hair Fall Defense 580ml\r\n\r\nInfused with Salon Grade Ionic Complex that recharges and makes hair 100% stronger, bringing them back to life\r\nRestores & nourishes your hair, thus protecting them from further damage and making them salon strong\r\nUse affter shampooing with TRESemme Ionic Strength Shampoo to reinforce protection from damage', '1500', '1200', '1693024791.jpg', '1', '1', '2023-08-25 22:39:51', '2023-08-26 02:03:13'),
(9, 5, 'Running Sneakers Men\'S Shoes', 'Running Sneakers Men\'S Shoes', 'Running Sneakers White Color Casual Lace-Up Shoes Winter And Summer Men\'S Shoes', 'Running Sneakers White Color Casual Lace-Up Shoes Winter And Summer Men\'S Shoes\r\n\r\nUpper Material: Synthetic\r\nFeature: Hard-Wearing, Massage, Breathable, Anti-Odor, Sweat-Absorbant\r\nClosure Type: Lace-Up\r\nOutsole Material: Rubber\r\nLining Material: Mesh\r\nSeason: Summer\r\nInsole Material: EVA\r\nPattern Type: Mixed Colors\r\nFit: Fits smaller than usual. Please check this store\'s sizing info\r\nShoe Type: Basic\r\nDepartment Name: Adult', '2200', '1850', '1693025149.jpg', '1', '1', '2023-08-25 22:45:49', '2023-08-25 22:45:49'),
(10, 5, 'Formal & Fashion Shoes For Party', 'Formal & Fashion Shoes For Party', 'New Premium Men\'s Shoe Stylish Formal & Fashion Shoes For Party Or Official Use Elegant', 'New Premium Men\'s Shoe Stylish Formal & Fashion Shoes For Party Or Official Use Elegant\r\nStyle: Formal & Fashion\r\nColor: Black\r\nGender: Men\r\nSize: 40/41//42/43\r\nMaterial: Rubber/Cotton\r\nSole Type: Soft PU Elegant\r\nLace: Formal & Fashion', '2500', '2100', '1693025251.jpg', '1', '1', '2023-08-25 22:47:31', '2023-08-25 22:47:31'),
(11, 6, 'Man fashionable watch', 'Man fashionable watch', 'Good Quality Man fashionable watch', 'Good Quality Man fashionable watch\r\n\r\nExclusive watch for man women\r\nSame as picture\r\nQuality is very good\r\nFamous brand\r\nMade in china\r\nWaterproof 100%', '1200', '850', '1693025362.jpg', '1', '1', '2023-08-25 22:49:22', '2023-08-25 22:49:22'),
(12, 6, 'eather Analog Watch For Men', 'eather Analog Watch For Men', 'Leather Analog Watch For Men - 4 - colour silver-black-white-blue', 'Leather Analog Watch For Men - 4 - colour silver-black-white-blue\r\n\r\n◉ Band Length : 26cm\r\n◉ Style : Business\r\n◉ Movement : Quartz\r\n◉ Water Resistance Depth : 3Bar\r\n◉ Clasp Type : Buckle\r\n◉ Origin : CN(Origin)\r\n◉ Case Material : Alloy\r\n◉ Case Thickness : 12.5mm\r\n◉ Boxes & Cases Material : Paper\r\n◉ Band Material Type : PU\r\n◉ Model Number : kesan mart', '1500', '1250', '1693025446.jpg', '1', '1', '2023-08-25 22:50:46', '2023-08-25 22:50:46'),
(13, 7, 'Silicone Spoon feeder', 'Silicone Spoon feeder', 'Silicone Spoon feeder Hard Spoon 90ml-1pcs', 'Silicone Spoon feeder Hard Spoon 90ml-1pcs \r\nSilicone Spoon feeder Hard Spoon 90ml-1pcs \r\nSilicone Spoon feeder Hard Spoon 90ml-1pcs', '500', '390', '1693025691.jpg', '1', '1', '2023-08-25 22:54:51', '2023-08-25 22:54:51'),
(14, 7, 'Baby Water Bottle', 'Baby Water Bottle', 'Baby Water Bottle - Food Grade Plastic, Mum Pot, Kids Water Bottle Double Handle', 'Baby Water Bottle - Food Grade Plastic, Mum Pot, Kids Water Bottle Double Handle \r\n\r\nItem Name: Kids Bottle \r\nColor: Multicolor \r\nEasy To Use \r\nBaby water bottle. \r\nFood grade plastic', '690', '490', '1693025804.jpg', '1', '1', '2023-08-25 22:56:44', '2023-08-25 22:56:44');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Women\'s Shoes', 1, '2023-08-25 22:08:27', '2023-08-25 22:44:03'),
(2, 'Bags', 1, '2023-08-25 22:08:42', '2023-08-25 22:08:42'),
(3, 'Skin Care', 2, '2023-08-25 22:28:50', '2023-08-25 22:28:50'),
(4, 'Hair Care', 2, '2023-08-25 22:29:08', '2023-08-25 22:29:08'),
(5, 'Men\'s Shoes', 3, '2023-08-25 22:42:03', '2023-08-25 22:43:46'),
(6, 'Watches', 3, '2023-08-25 22:42:41', '2023-08-25 22:42:41'),
(7, 'Feeding', 4, '2023-08-25 22:52:46', '2023-08-25 22:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_as` tinyint NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role_as`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$xiFoijQCMccdNLZ9187HJuGJP62xvEqV006dikj5lHOU8ppW2/uEO', 1, NULL, '2023-08-25 22:05:08', '2023-08-25 22:05:08'),
(2, 'user', 'user@gmail.com', NULL, '$2y$10$gR2QWuQQv3R90a7MaUfGtuS2qStZGKkLYLhUsyka96Wz.oNVsJL4y', 0, NULL, '2023-08-25 22:05:47', '2023-08-25 22:05:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
