
@extends('layouts.app')

@section('content')
    <div class="container d-flex">
        <a class="m-4" href="{{ route('categories.index') }}">All Categories</a>
        <a class="m-4" href="{{ route('subcategories.index') }}">All Sub Categories</a>
        <a class="m-4" href="{{ route('products.index') }}">All Products</a>
    </div>
    <div class="container">
        <div class="card shadow product_data mb-5">
            <div class="card-body">
                @if (Session::has('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="row">
                    <div class="class mx-auto">
                        <h2>
                            Product List
                            <a href="{{ route('products.create') }}" class="btn btn-sm btn-primary float-end">Add New Product</a>
                        </h2>
                        <hr>
                    </div>
                    <div class="mx-auto">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Sub Category</th>
                                    <th>Name</th>
                                    <th>Selling Price</th>
                                    <th>Image</th>
                                    <th>Actions</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $product->subcategory->name }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->selling_price }}</td>
                                        <td>
                                            <img src="{{ asset('assets/uploads/product/'.$product->image) }}" alt="Image Loading" style="width:80px; height:80px">
                                        </td>
                                        <td colspan="2" >
                                            <a class="btn btn-sm btn-success" href="{{ route('products.show', $product->id) }}">Details</a>
                                            <a class="btn btn-sm btn-info" href="{{ route('products.edit', $product->id) }}">Edit</a>

                                            <form class="d-inline" action="{{ route('products.destroy', $product->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


