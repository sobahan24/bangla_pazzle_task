@extends('layouts.app')

@section('content')
    <div class="container d-flex">
        <a class="m-4" href="{{ route('categories.index') }}">All Categories</a>
        <a class="m-4" href="{{ route('subcategories.index') }}">All Sub Categories</a>
        <a class="m-4" href="{{ route('products.index') }}">All Products</a>
    </div>
    <div class="container card">

        <h2 class="mx-3 mt-3">Edit Product</h2>
        <hr>

        <div class="card-body">
            <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12 mb-3">
                    <label class="form-label fw-bold" for="">Sub Category Name:</label>
                    <select name="subcategory_id" id="subcategory_id" class="form-select">
                        @foreach ($subcategories as $subcategory)
                            <option value="{{ $subcategory->id }}" {{ $subcategory->id === $product->subcategory_id ? 'selected' : '' }}>
                                {{ $subcategory->name }}
                            </option>
                        @endforeach
                    </select>
                    </div>

                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ $product->name }}">
                    </div>

                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Slug:</label>
                        <input type="text" class="form-control" name="slug" value="{{ $product->slug }}">
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="form-label fw-bold" for="">Small Description:</label>
                        <textarea name="small_description" rows="2" class="form-control"> {{ $product->small_description }}</textarea>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label class="form-label fw-bold" for="">Description:</label>
                        <textarea name="description" rows="2" class="form-control">{{ $product->description }}</textarea>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Original Price:</label>
                        <input type="number" name="original_price" class="form-control" value="{{ $product->original_price }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Selling Price:</label>
                        <input type="number" name="selling_price" class="form-control" value="{{ $product->selling_price }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Tax:</label>
                        <input type="number" name="tax" class="form-control" value="{{ $product->tax }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Quantity:</label>
                        <input type="number" name="qty" class="form-control" value="{{ $product->qty }}">
                    </div>
                    @if ($product->image)
                        <img src="{{ asset('assets/uploads/product/'.$product->image) }}" alt="" style="width:200px; height:200px">
                    @endif
                    <div class="col-md-12">
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="col-md-12 mt-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection


