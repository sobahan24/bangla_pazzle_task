@extends('layouts.app')

@section('content')
    <div class="container d-flex">
        <a class="m-4" href="{{ route('categories.index') }}">All Categories</a>
        <a class="m-4" href="{{ route('subcategories.index') }}">All Sub Categories</a>
        <a class="m-4" href="{{ route('products.index') }}">All Products</a>
    </div>

    <div class="container">
        <div class="card shadow product_data mb-5">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 border-right">
                        <img style="height: 300px" src="{{ asset('assets/uploads/product/'.$products->image) }}" class="w-100" alt="">
                    </div>
                    <div class="col-md-8">
                        <h2 class="mb-0">
                            {{ $products->name }}
                        </h2>
                        <hr>
                        <label class="me-3" for="">Original Price : <s style="color: red">TK {{ $products->original_price }}</s></label>
                        <label class="fw-bold" for="">Selling Price : TK {{ $products->selling_price }}</label>
                        <p class="mt-3">
                            {{ $products->small_description }}
                        </p>
                        <hr>
                        <div class="row mt-2">
                            <div class="col-md-2">
                                <input type="hidden" value="{{ $products->id }}" class="product_id mb-2">
                                <label for="Quantity">Quantity</label>
                                <div class="input-group text-center mt-2" style="width: 105px;">
                                    <button class="input-group-text decrement-btn">-</button>
                                    <input type="text" name="Quantity" value="{{ $products->qty }}" class="form-control qty-input">
                                    <button class="input-group-text increment-btn">+</button>
                                </div>
                            </div>
                            <div class="col-md-10 mt-2">
                                <br/>

                                <button type="submit" class="btn btn-primary me-3 float-start">Add to Cart  <i class="fa fa-shopping-cart"></i></button>
                                <button type="button" class="btn btn-success me-3 float-start">Add to Wishlist  <i class="fa fa-heart"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <h3 class="p-3">Product Description</h3>
                <p class="ps-3">
                    {{  $products->description  }}
                </p>
            </div>
        </div>
    </div>
@endsection
