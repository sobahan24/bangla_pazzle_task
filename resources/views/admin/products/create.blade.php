@extends('layouts.app')

@section('content')
    <div class="container d-flex">
        <a class="m-4" href="{{ route('categories.index') }}">All Categories</a>
        <a class="m-4" href="{{ route('subcategories.index') }}">All Sub Categories</a>
        <a class="m-4" href="{{ route('products.index') }}">All Products</a>
    </div>
    <div class="container card">

        <h2 class="mx-3 mt-3">Add Product</h2>
        <hr>

        <div class="card-body">
            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">

                    <div class="col-md-12 mb-3">
                        <select class="form-select" name="subcategory_id">
                            <option value="">Select a Sub Category *</option>
                            @foreach ($subcategories as $item )
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Name:</label>
                        <input type="text" class="form-control" name="name">
                    </div>

                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Slug:</label>
                        <input type="text" class="form-control" name="slug">
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="form-label fw-bold" for="">Small Description:</label>
                        <textarea name="small_description" rows="2" class="form-control"></textarea>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label class="form-label fw-bold" for="">Description:</label>
                        <textarea name="description" rows="2" class="form-control"></textarea>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Original Price:</label>
                        <input type="number" name="original_price" class="form-control">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Selling Price:</label>
                        <input type="number" name="selling_price" class="form-control">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Tax:</label>
                        <input type="number" name="tax" class="form-control">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label fw-bold" for="">Quantity:</label>
                        <input type="number" name="qty" class="form-control">
                    </div>

                    <div class="col-md-12">
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="col-md-12 mt-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection


