@extends('layouts.app')

@section('content')
    <div class="container d-flex">
        <a class="m-4" href="{{ route('categories.index') }}">All Categories</a>
        <a class="m-4" href="{{ route('subcategories.index') }}">All Sub Categories</a>
        <a class="m-4" href="{{ route('products.index') }}">All Products</a>
    </div>
    <div class="container">
        <div class="card shadow product_data mb-5">
            <div class="card-body">
                @if (Session::has('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="row">
                    <div class="class mx-auto">
                        <h2>
                            Category List
                            <a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary float-end">Add New Category</a>
                        </h2>
                        <hr>
                    </div>
                    <div class="mx-auto">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td class="d-flex">
                                            <a class="btn btn-sm btn-info me-3" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                                            <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

