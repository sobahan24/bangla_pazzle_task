@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Admin Dashboard</h2>
        <hr>
    </div>
    <div class="container d-flex">
        <a class="m-4" href="{{ route('categories.index') }}">All Categories</a>
        <a class="m-4" href="{{ route('subcategories.index') }}">All Sub Categories</a>
        <a class="m-4" href="{{ route('products.index') }}">All Products</a>
    </div>
@endsection
