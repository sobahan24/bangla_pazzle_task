@extends('layouts.app')

@section('content')
<div class="container">
    <!-- ============= COMPONENT ============== -->
    <div class="container">
        <!-- ============= COMPONENT ============== -->
        <h2>Front View</h2>
        <hr>
        @foreach ($productsAll as $productsA)

        @endforeach

        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown"> Categories </a>
                            <ul class="dropdown-menu">
                                @foreach ($categories as $category)
                                <li><a class="dropdown-item" href="#"> {{ $category->name }} &raquo; </a>
                                    <ul class="submenu dropdown-menu">
                                        @foreach ($subcategoriesAll as $subcategoriesA)
                                        @if($category->id==$subcategoriesA->category_id)
                                        <li>
                                            <a class="dropdown-item" href="{{ url('view-products/'.$subcategoriesA->id) }}">{{ $subcategoriesA->name }}</a>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div> <!-- navbar-collapse.// -->
            </div> <!-- container-fluid.// -->
        </nav>




        <!-- ============= COMPONENT END// ============== -->
    </div><!-- container //  -->

    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5>{{ 'Subcategory: '.$subcategories->name }}</h5>
                    <div class="row">
                        @foreach ($products as $product)
                            @if($subcategories->id==$product->subcategory_id)
                            <div class="col-md-3 mb-3">
                                <a class="catList" href="{{ url('product-details/'.$product->id) }}">
                                    <div class="card shadow py-3">
                                        <img style="height: 250px" src="{{ asset('assets/uploads/product/'.$product->image) }}" alt="Product Image">
                                        <div class="card-body text-center">
                                            <h5>{{ $product->name }}</h5>
                                            <span class="float-start">TK {{ $product->selling_price }}</span>
                                            <span class="float-end"><s>TK {{ $product->selling_price }}</s></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============= COMPONENT END// ============== -->
</div><!-- container //  -->
@endsection