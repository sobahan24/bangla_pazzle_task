@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- ============= COMPONENT ============== -->
        <h2>Front View</h2>
        <hr>
        @foreach ($products as $product )

        @endforeach
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown"> Categories </a>
                            <ul class="dropdown-menu">
                                @foreach ($categories as $category)
                                    <li><a class="dropdown-item" href="#"> {{ $category->name }} &raquo; </a>
                                        <ul class="submenu dropdown-menu">
                                            @foreach ($subcategories as $subcategory)
                                                @if($category->id==$subcategory->category_id)
                                                    <li>
                                                        <a class="dropdown-item" href="{{ url('view-products/'.$subcategory->id) }}">{{ $subcategory->name }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div> <!-- navbar-collapse.// -->
            </div> <!-- container-fluid.// -->
        </nav>
        <!-- ============= COMPONENT END// ============== -->
    </div><!-- container //  -->
@endsection
